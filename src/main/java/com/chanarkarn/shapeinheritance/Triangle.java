/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeinheritance;

/**
 *
 * @author A_R_T
 */
public class  Triangle extends Shape {

    private double base, height;

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }

    public double trgArea() {
        return base * height * 0.5;
    }

    public void print() {
        System.out.println("Area of triangle = is " + trgArea());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeinheritance;

/**
 *
 * @author A_R_T
 */
public class Square extends Shape {

    private double side;

    public Square(double side) {

        this.side = side;
    }

    public double squArea() {
        return side * side;
    }

    public void print() {
        System.out.println("Area of square = is " + squArea());
    }
}

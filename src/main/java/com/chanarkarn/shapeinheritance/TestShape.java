/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeinheritance;

/**
 *
 * @author A_R_T
 */
public class TestShape {

    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.print();

        Triangle triangle = new Triangle(3, 4);
        triangle.print();
        
        Rectangle rectangle = new Rectangle(4, 3);
        rectangle.print();
        
        Square square = new Square(3);
        square.print();
    }

}

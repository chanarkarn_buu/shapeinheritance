/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeinheritance;

/**
 *
 * @author A_R_T
 */
public class Circle extends Shape {

    private double r;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    public double calArea() {
        return Math.PI * r * r;
    }

    public double getR() {
        return r;
    }

    public void print() {
        System.out.println("Area of circlel(r = " + getR() + ") is " + calArea());
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.shapeinheritance;

/**
 *
 * @author A_R_T
 */
public class Rectangle extends Shape {

    private double width, height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    public double recArea() {
        return width * height;
    }

    public void print() {
        System.out.println("Area of rectangle = is " + recArea());
    }
}
